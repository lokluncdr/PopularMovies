package com.example.lok.popularmovies.data;

import net.simonvt.schematic.annotation.AutoIncrement;
import net.simonvt.schematic.annotation.DataType;
import net.simonvt.schematic.annotation.NotNull;
import net.simonvt.schematic.annotation.PrimaryKey;

/**
 * Created by Lok on 18/01/17.
 * https://valeriodg.com/2016/06/09/contentprovider-example-15-minutes/
 */

public interface FavouriteColumns {

    //FIELDS
    @DataType(DataType.Type.INTEGER)
    @PrimaryKey
    @AutoIncrement
    public static final String _ID = "_id";

    @DataType(DataType.Type.INTEGER) //The type of the value that we're storing in this column
    @NotNull //Constraint, the app will thrown an exception if we try to store a null value
    public static final String FavouriteId = "favouriteId"; //The name of the column

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String Title = "title";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String ReleaseDate = "releaseDate";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String PosterPath = "posterPath";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String VoteAverage = "voteAverage";

    @DataType(DataType.Type.TEXT)
    @NotNull
    public static final String Overview = "overview";


}

//    private String id;
//    private String title;
//    private String release_date;
//    private String poster_path;
//    private String vote_average;
//    private String overview;