package com.example.lok.popularmovies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lok.popularmovies.adapters.ReviewAdapter;
import com.example.lok.popularmovies.adapters.TrailerAdapter;
import com.example.lok.popularmovies.asynctasks.MovieAsyncTask;
import com.example.lok.popularmovies.pojo.Movie;
import com.example.lok.popularmovies.pojo.MovieReview;
import com.example.lok.popularmovies.pojo.MovieTrailer;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;


/**
 * A fragment representing a single Movie detail screen.
 * This fragment is either contained in a {@link MovieListActivity}
 * in two-pane mode (on tablets) or a {@link MovieDetailActivity}
 * on handsets.
 */
public class MovieDetailFragment extends Fragment implements MovieAsyncTask.MovieTrailersAsyncResponse, MovieAsyncTask.MovieReviewsAsyncResponse {
    //FIELDS
    private final String FRAGMENT_TAG_PORTRAIT = "popular_movies_portrait";
    private final String FRAGMENT_TAG_LANDSCAPE = "popular_movies_landscape";
    private final String MOVIE = "Movie";
    private final String _TRAILER = "trailer";
    private final String _REVIEW = "review";
    private final String REMOVE_FRAGMENT = "remove_fragment";
    private final String SCROLLVIEW_POSITION = "scrollview_position";
    private final String SCROLLVIEW_HEIGHT = "scrollview_height";
    boolean mReview = false;
    boolean mTrailer = false;
    private int mHeightOfListViews;
    private Activity activity;
    private Movie movie;
    private ImageView favouriteImageView;
    private List<MovieTrailer> movieTrailerList;
    private List<MovieReview> movieReviewsList;
    private ShareActionProvider mShareActionProvider;
    private Intent shareIntent;
    private Bundle bundle;
    private boolean mTwoPane;
    private boolean removeFragment;
    private Context context;
    private NestedScrollView mScrollView;
    private int[] mOldScrollPositionArray;
    private int mOldScrollViewHeight;
    private double newScrollPosition;
    private ListView movieTrailerListView;
    private ListView movieReviewListView;

    //CONSTRUCTOR

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MovieDetailFragment() {
    }

    //METHODS
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this.getActivity();
        //For saving and setting scrollview position
        mOldScrollPositionArray = new int[2];

        MovieDetailFragment fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_PORTRAIT);
        //When in portrait mode set twopane to false, otherwise if in landscape mode set it to true
        if (fragment != null) {
            mTwoPane = false;

            //For saving and setting scrollview position
            mScrollView = (NestedScrollView) activity.findViewById(R.id.movie_detail_container);
        } else {
            fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_LANDSCAPE);

            if (fragment != null) {
                mTwoPane = true;

            }
            //After switching to portrait after being in landscape
            //Check what has been saved in removeFragment in onSaveInstanceState
            if (savedInstanceState != null) {
                removeFragment = savedInstanceState.getBoolean(REMOVE_FRAGMENT);
                //Remove this current fragment
                if (mTwoPane && removeFragment) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .remove(fragment)
                            .commit();
                }
            }
        }

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            setHasOptionsMenu(true);

            context = getContext();

            //Retrieve Movie for Detail
            bundle = getActivity().getIntent().getBundleExtra(MOVIE);
            if (bundle == null) {
                bundle = getArguments();
            }
            movie = bundle.getParcelable(MOVIE);

            //Get Trailers & Reviews for this movie
            if (Utility.isNetworkAvailable(context)) {
                //Retrieve MovieTrailers with AsyncTask
                MovieAsyncTask movieTrailersAsyncTask = new MovieAsyncTask((MovieAsyncTask.MovieTrailersAsyncResponse) this, movie.getId());
                movieTrailersAsyncTask.execute(_TRAILER);

                //Retrieve MovieReviews with AsyncTask
                MovieAsyncTask movieReviewsAsyncTask = new MovieAsyncTask((MovieAsyncTask.MovieReviewsAsyncResponse) this, movie.getId());
                movieReviewsAsyncTask.execute(_REVIEW);
            } else {
                Toast.makeText(getActivity(), "There's no internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            //When in landscape mode. With "Master/Detail" view and user switches back to portrait mode, don't execute the code below
            //Retrieve Movie for title
            bundle = getActivity().getIntent().getBundleExtra(MOVIE);
            if (bundle == null) {
                bundle = getArguments();
                movie = bundle.getParcelable(MOVIE);
            }

            //Set title
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(movie.getTitle());
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save viewstate for title when user rotates screen
        outState.putParcelable(MOVIE, movie);

        if (!mTwoPane) { //Enter when in portrait mode
            //Save scroll position as well
            outState.putIntArray(SCROLLVIEW_POSITION, new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
            outState.putInt(SCROLLVIEW_HEIGHT, mScrollView.getChildAt(0).getMeasuredHeight());

        }
        MovieDetailFragment fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_PORTRAIT);
        //Only remove when in master/detail flow
        //NO PORTRAIT MODE exists
        if (fragment == null) {
            fragment = (MovieDetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG_LANDSCAPE);
            //LANDSCAPE MODE exists
            if (fragment != null) {
                removeFragment = true;
                outState.putBoolean(REMOVE_FRAGMENT, removeFragment);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.detail_menu, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.action_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        //Set up the intent
        shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

        // Add data to the intent, the receiving app will decide what to do with it.
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.shareintent_subject));

        String trailerURL = null;
        if (movieTrailerList == null) {
            trailerURL = "of " + movie.getTitle();
        } else {
            trailerURL = "https://www.youtube.com/watch?v=" + movieTrailerList.get(0).getKey();
        }

        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.shareintent_text) + " " + trailerURL);
        mShareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.movie_detail, container, false);

        //Only execute code below when fragment wasn't removed yet
        if (!removeFragment) {
            //Set Image
            ImageView movieImageView = (ImageView) rootView.findViewById(R.id.movie_detail_poster);
            Picasso.with(context).load("http://image.tmdb.org/t/p/w342/" + movie.getPoster_path()).into(movieImageView);

            //Set Release Date, Vote Average and Overview
            ((TextView) rootView.findViewById(R.id.movie_detail_release_date)).setText(movie.getRelease_date());
            ((TextView) rootView.findViewById(R.id.movie_detail_vote_average)).setText(Utility.roundOff(movie.getVote_average()));
            ((TextView) rootView.findViewById(R.id.movie_detail_overview)).setText(movie.getOverview());

            //Set Favourite
            favouriteImageView = (ImageView) rootView.findViewById(R.id.movie_detail_favourite);
            if (Utility.isInDatabase(context, movie)) {
                favouriteImageView.setImageResource(R.drawable.ic_star_black_48dp);
                favouriteImageView.setTag(R.drawable.ic_star_black_48dp);
            } else {
                favouriteImageView.setImageResource(R.drawable.ic_star_border_black_48dp);
                favouriteImageView.setTag(R.drawable.ic_star_border_black_48dp);
            }
            //Mark as favouriteImageView
            favouriteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (favouriteImageView.getTag().equals(R.drawable.ic_star_border_black_48dp)) {
                        //Show that user clicked on item
                        Animation animation = new AlphaAnimation(0.3f, 1.0f);
                        animation.setDuration(200);
                        v.startAnimation(animation);

                        favouriteImageView.setImageResource(R.drawable.ic_star_black_48dp);
                        favouriteImageView.setTag(R.drawable.ic_star_black_48dp);
                        //Save Favourite to Shared Preferences
                        Utility.saveToDatabase(context, movie);
                    } else {
                        favouriteImageView.setImageResource(R.drawable.ic_star_border_black_48dp);
                        favouriteImageView.setTag(R.drawable.ic_star_border_black_48dp);
                        //Remove Favourite from Shared Preference
                        Utility.removeFromDatabase(context, movie);
                    }
                }
            });

            //Retrieve listview position
            if (savedInstanceState != null && !mTwoPane) { //Enter when in Portrait mode
                mOldScrollPositionArray = savedInstanceState.getIntArray(SCROLLVIEW_POSITION);
                mOldScrollViewHeight = savedInstanceState.getInt(SCROLLVIEW_HEIGHT);
            }
        }
        return rootView;
    }

    public void addTrailers(ListView trailersListView,
                            final List<MovieTrailer> movieTrailerList) {
        TrailerAdapter movieTrailerAdapter;
        if (!movieTrailerList.isEmpty()) {
            movieTrailerAdapter = new TrailerAdapter(getActivity(), movieTrailerList);
        } else {
            //Let user know there are no trailers, by appropriating the list with an empty movie
            movieTrailerList.add(new MovieTrailer("00000", "00000", "00000", "00000"));
            movieTrailerAdapter = new TrailerAdapter(getActivity(), movieTrailerList);
        }
        trailersListView.setAdapter(movieTrailerAdapter);
        //Show whole listview regardless of it being in a scrollview...
        int height = Utility.setListViewHeightBasedOnChildren(trailersListView);

        setScrollViewPosition(_TRAILER, height);
    }

    public void addReviews(ListView reviewsListView, final List<MovieReview> movieReviewList) {
        ReviewAdapter movieReviewAdapter;

        if (!movieReviewList.isEmpty()) {
            movieReviewAdapter = new ReviewAdapter(getActivity(), movieReviewList);
        } else {
            //Let user know there are no reviews, by appropriating the list with an empty movie
            movieReviewList.add(new MovieReview("00000", "00000"));
            movieReviewAdapter = new ReviewAdapter(getActivity(), movieReviewList);
        }
        reviewsListView.setAdapter(movieReviewAdapter);
        //Show whole listview regardless of it being in a scrollview...
        int height = Utility.setListViewHeightBasedOnChildren(reviewsListView);

        setScrollViewPosition(_REVIEW, height);
    }

    public void setScrollViewPosition(String movieTrivia, int height) {

        //Only enter when screen was rotated and former mOldScrollPositionArray and mOldScrollViewHeight was saved before
        if (mOldScrollPositionArray != null && mOldScrollViewHeight != 0) {
            if (movieTrivia == _TRAILER) {
                mTrailer = true;
                mHeightOfListViews += height;
            } else if (movieTrivia == _REVIEW) {
                mReview = true;
                mHeightOfListViews += height;
            }
            if (mReview && mTrailer) {
                //Don't collapse when screen was rotated
                AppBarLayout appBar = (AppBarLayout) activity.findViewById(R.id.app_bar);
                appBar.setExpanded(false);

                int total = 100; //Total is 100%

                //Convert correctly. Calculate percentage of former proportions
                //(Scrollviewposition / Height ) * 100 = percentage
                double percentage = ((double) mOldScrollPositionArray[1] / (double) mOldScrollViewHeight);
                percentage = percentage * total;

                //Retrieve current height of scrollview
                mScrollView = (NestedScrollView) activity.findViewById(R.id.movie_detail_container);
                double newScrollViewHeight = mScrollView.getChildAt(0).getHeight();
                //Add the height of both MovieTrailers- and MovieReviews ListView
                newScrollViewHeight = newScrollViewHeight + mHeightOfListViews;

                //Convert correctly. Calculate percentage to correct position for current proportions
                //(percentage /100)* (current)Height = Scrollviewposition
                newScrollPosition = (percentage / total) * newScrollViewHeight;

                double correction;

                //Add half of screen for correction purposes....
                int orientation = this.getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    correction = 1.20; //1.20, just because... after lots of trial and error this gives the best experience

                    //Portrait
                    final int viewWidth = mScrollView.getChildAt(0).getWidth();
                    newScrollPosition = newScrollPosition - (viewWidth * correction);
                } else {
                    correction = 5; //5, just because... after lots of trial and error this gives the best experience

                    //Landscape
                    final int viewHeight = mScrollView.getChildAt(0).getHeight();
                    newScrollPosition = newScrollPosition - (viewHeight / correction);
                }

                if (newScrollPosition <= newScrollViewHeight) {

                    mScrollView.post(new Runnable() {
                        public void run() {
                            mScrollView.scrollTo(mOldScrollPositionArray[0], (int) newScrollPosition);
                            mReview = false;
                            mTrailer = false;
                        }
                    });
                }
            }
        }
    }

    @Override
    public void processFinishMovieTrailers(JSONObject movietrailers) {
        //After trailers are retrieved, make ArrayList<MovieTrailer> out of it
        movieTrailerList = Utility.getMovieTrailersFromJSON(movietrailers);

        movieTrailerListView = (ListView) getActivity().findViewById(R.id.listview_trailer);
        //Send list to listview
        addTrailers(movieTrailerListView, movieTrailerList);
    }

    @Override
    public void processFinishMovieReviews(JSONObject moviereviews) {
        //After trailers are retrieved, make ArrayList<MovieReview> out of it
        movieReviewsList = Utility.getMovieReviewsFromJSON(moviereviews);

        movieReviewListView = (ListView) getActivity().findViewById(R.id.listview_review);
        //Send list to listview
        addReviews(movieReviewListView, movieReviewsList);
    }
}
