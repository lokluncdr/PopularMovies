package com.example.lok.popularmovies.asynctasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.lok.popularmovies.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Lok on 09/12/16.
 */

public class MovieAsyncTask extends AsyncTask<String, Void, JSONObject> {
    //FIELDS
    public final String LOG_TAG = MovieAsyncTask.class.getSimpleName();
    //Verify what to retrieve
    private final String _POPULAR = "popular";
    private final String _TOP_RATED = "toprated";
    private final String _TRAILER = "trailer";
    private final String _REVIEW = "review";
    //For building the URI
    private final String POPULAR = "movie/popular";
    private final String TOPRATED = "movie/top_rated";
    private final String MOVIE = "movie/";
    private final String VIDEOS = "/videos";
    private final String REVIEWS = "/reviews";

    public MoviesAsyncResponse delegateMovies = null;
    public MovieTrailersAsyncResponse delegateMovieTrailers = null;
    public MovieReviewsAsyncResponse delegateMovieReviews = null;

    private JSONObject json;
    private String movieId;

    private static final String API_KEY = BuildConfig.POPULAR_MOVIES_API_KEY;


    //CONSTRUCTORS
    //Movies
    public MovieAsyncTask(MoviesAsyncResponse delegate) {
        this.delegateMovies = delegate;
    }

    //MovieTrailers
    public MovieAsyncTask(MovieTrailersAsyncResponse delegate, String id) {
        this.delegateMovieTrailers = delegate;
        movieId = id;
    }

    //MovieReviews
    public MovieAsyncTask(MovieReviewsAsyncResponse delegate, String id) {
        this.delegateMovieReviews = delegate;
        movieId = id;
    }

    //METHODS

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected JSONObject doInBackground(String... params) {

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String jsonString = null;
        String format = "json";

        try {
            final String FORECAST_BASE_URL = "http://api.themoviedb.org/3";
            String MOVIES_PARAM = "";

            //Build the right uri
            switch (params[0]) {
                case _POPULAR:
                    //Popular Movies
                    // Construct the URL for the Popular Movies query
                    // ->http://api.themoviedb.org/3/movie/popular?api_key=##############
                    MOVIES_PARAM = POPULAR;
                    break;
                case _TOP_RATED:
                    //Top Rated Movies
                    // Construct the URL for the Top Rated Movies query
                    // ->http://api.themoviedb.org/3/movie/top_rated?api_key=##############
                    MOVIES_PARAM = TOPRATED;
                    break;
                case _TRAILER:
                    //MovieTrailers
                    // Construct the URL for the MovieTrailers query
                    // ->https://api.themoviedb.org/3/movie/284052/videos?api_key=##############&language=en-US
                    MOVIES_PARAM = MOVIE + movieId + VIDEOS; //params[0] is id for/movie/{id}/videos
                    break;
                case _REVIEW:
                    //MovieReviews
                    // Construct the URL for the MovieReviews query
                    // ->https://api.themoviedb.org/3/movie/259316/reviews?api_key=##############&language=en-US
                    MOVIES_PARAM = MOVIE + movieId + REVIEWS; //params[0] is id for/movie/{id}/videos. Endpoint is /movie/{id}/reviews
                    break;
            }

            final String APPID_PARAM = "api_key";

            Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendEncodedPath(MOVIES_PARAM)
                    .appendQueryParameter(APPID_PARAM, API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            jsonString = buffer.toString();
            json = getJsonFromString(jsonString);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the movie data, there's no point in attemping
            // to parse it.
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return json;
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param json The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(JSONObject json) {
        super.onPostExecute(json);

        if (delegateMovies != null) {
            delegateMovies.processFinishMovies(json);
        } else if (delegateMovieTrailers != null) {
            delegateMovieTrailers.processFinishMovieTrailers(json);
        } else if (delegateMovieReviews != null) {
            delegateMovieReviews.processFinishMovieReviews(json);
        }
    }

    private JSONObject getJsonFromString(String jsonString) {
        // Now we have a String representing the complete movies (Popular or Top Rated),
        // movietrailers or moviereviews in JSON Format.
        // Fortunately parsing is easy:  constructor takes the JSON string and converts it
        // into an Object hierarchy
        try {
            json = new JSONObject(jsonString);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return json;
    }

    //INTERFACES
    //Movies
    public interface MoviesAsyncResponse {
        void processFinishMovies(JSONObject output);
    }

    //MovieTrailers
    public interface MovieTrailersAsyncResponse {
        void processFinishMovieTrailers(JSONObject output);
    }

    //MovieReviews
    public interface MovieReviewsAsyncResponse {
        void processFinishMovieReviews(JSONObject output);
    }
}
