package com.example.lok.popularmovies;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lok.popularmovies.data.FavouriteColumns;
import com.example.lok.popularmovies.data.FavouriteProvider;
import com.example.lok.popularmovies.pojo.Movie;
import com.example.lok.popularmovies.pojo.MovieReview;
import com.example.lok.popularmovies.pojo.MovieTrailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lok on 25/11/16.
 */

public class Utility {
    //METHODS
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static List<Movie> getMoviesFromJSON(JSONObject moviesJson) {

        final String PM_ID = "id";
        final String PM_TITLE = "title";
        final String PM_RELEASE_DATE = "release_date";
        final String PM_VOTE_AVERAGE = "vote_average";
        final String PM_OVERVIEW = "overview";
        final String PM_POSTER_PATH = "poster_path";

        ArrayList<Movie> mMovieList = new ArrayList<Movie>();
        JSONArray moviesArray = null;
        try {
            moviesArray = moviesJson.getJSONArray("results");

            for (int i = 0; i < moviesArray.length(); i++) {
                // These are the values that will be collected.
                String id;
                String title;
                String release_date;
                String vote_average;
                String overview;
                String poster_path;

                //Get JsonObject representing the Movie
                JSONObject movie = moviesArray.getJSONObject(i);

                id = movie.getString(PM_ID);
                title = movie.getString(PM_TITLE);
                release_date = movie.getString(PM_RELEASE_DATE);
                vote_average = movie.getString(PM_VOTE_AVERAGE);
                overview = movie.getString(PM_OVERVIEW);
                poster_path = movie.getString(PM_POSTER_PATH);

                mMovieList.add(new Movie(id, title, release_date, vote_average, overview, poster_path));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mMovieList;
    }

    public static String roundOff(String vote_average) {
        double average = Double.parseDouble(vote_average);
        average = new BigDecimal(average).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
        return String.valueOf(average) + "/10";
    }

    public static int setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return 0;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

        return params.height;
    }

    public static List<MovieTrailer> getMovieTrailersFromJSON(JSONObject trailersJson) {
        final String PM_ID = "id";
        final String PM_KEY = "key";
        final String PM_NAME = "name";
        final String PM_TYPE = "type";

        ArrayList<MovieTrailer> mMovieTrailers = new ArrayList<>();
        JSONArray trailersArray = null;
        try {
            trailersArray = trailersJson.getJSONArray("results");

            for (int i = 0; i < trailersArray.length(); i++) {
                // These are the values that will be collected.
                String id;
                String key;
                String name;
                String type;

                //Get JsonObject representing the Movie
                JSONObject trailer = trailersArray.getJSONObject(i);

                id = trailer.getString(PM_ID);
                key = trailer.getString(PM_KEY);
                name = trailer.getString(PM_NAME);
                type = trailer.getString(PM_TYPE);

                mMovieTrailers.add(new MovieTrailer(id, key, name, type));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mMovieTrailers;
    }

    public static List<MovieReview> getMovieReviewsFromJSON(JSONObject reviewsJson) {

        final String PM_AUTHOR = "author";
        final String PM_CONTENT = "content";

        ArrayList<MovieReview> mMovieMovieReviews = new ArrayList<>();
        JSONArray trailersArray = null;
        try {
            trailersArray = reviewsJson.getJSONArray("results");

            for (int i = 0; i < trailersArray.length(); i++) {
                // These are the values that will be collected.
                String author;
                String content;

                //Get JsonObject representing the Movie
                JSONObject trailer = trailersArray.getJSONObject(i);

                author = trailer.getString(PM_AUTHOR);
                content = trailer.getString(PM_CONTENT);

                mMovieMovieReviews.add(new MovieReview(author, content));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mMovieMovieReviews;
    }

    public static void saveToDatabase(Context context, Movie movie) {
        if (!isInDatabase(context, movie)) {
            //Store values that the ContentResolver can process
            ContentValues values = new ContentValues();
            values.put(FavouriteColumns.FavouriteId, movie.getId());
            values.put(FavouriteColumns.Title, movie.getTitle());
            values.put(FavouriteColumns.ReleaseDate, movie.getRelease_date());
            values.put(FavouriteColumns.PosterPath, movie.getPoster_path());
            values.put(FavouriteColumns.VoteAverage, movie.getVote_average());
            values.put(FavouriteColumns.Overview, movie.getOverview());

            Uri uri = context.getContentResolver().insert(FavouriteProvider.Favourites.CONTENT_URI, values);
            Toast.makeText(context, R.string.saved, Toast.LENGTH_SHORT).show();
        }
    }

    public static void removeFromDatabase(Context context, Movie movie) {
        ContentResolver cr = context.getContentResolver();
        String where = "favouriteId=?";
        String[] args = new String[]{movie.getId()};
        cr.delete(FavouriteProvider.Favourites.CONTENT_URI, where, args);
    }

    public static boolean isInDatabase(Context context, Movie movie) {
        String[] requestedColumns = {
                FavouriteColumns.FavouriteId,
                FavouriteColumns.Title,
                FavouriteColumns.ReleaseDate,
                FavouriteColumns.PosterPath,
                FavouriteColumns.VoteAverage,
                FavouriteColumns.Overview
        };

        Cursor c = context.getContentResolver().query(
                FavouriteProvider.Favourites.CONTENT_URI, //Uri, The URI, using the content:// scheme, for the content to retrieve
                requestedColumns, //projection, pass all columns
                FavouriteColumns.FavouriteId + "='" + movie.getId() + "'",  //selection, A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given URI.
                null, //selectionArgs
                null); //sortOrder

        c.moveToFirst();
        if (c != null) {
            if (c.getCount() == 1) {
                // already inserted
                c.close();
                return true;
            }
        }
        // row does not exist
        return false;
    }

    public static ArrayList<Movie> retrieveFavouritesFromDB(Context context) {
        ArrayList<Movie> movieList = new ArrayList<>();

        String[] requestedColumns = {
                FavouriteColumns.FavouriteId,
                FavouriteColumns.Title,
                FavouriteColumns.ReleaseDate,
                FavouriteColumns.PosterPath,
                FavouriteColumns.VoteAverage,
                FavouriteColumns.Overview
        };

        Cursor c = context.getContentResolver().query(
                FavouriteProvider.Favourites.CONTENT_URI, //Uri, The URI, using the content:// scheme, for the content to retrieve
                requestedColumns, //projection, pass all columns
                null,  //selection, A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given URI.
                null, //selectionArgs
                null); //sortOrder

        c.moveToFirst();
        for (int i = 0; i != c.getCount(); i++) {
            //Convert cursor to Movie object
            Movie movie = populateMovie(c);
            //Add movie to list
            movieList.add(movie);
            c.moveToNext();
        }
        c.close();
        return movieList;
    }

    public static Movie populateMovie(Cursor cursor) {
        Movie m = new Movie();

        try {
            m.setId(cursor.getString(cursor.getColumnIndex(FavouriteColumns.FavouriteId)));
            m.setTitle(cursor.getString(cursor.getColumnIndex(FavouriteColumns.Title)));
            m.setRelease_date(cursor.getString(cursor.getColumnIndex(FavouriteColumns.ReleaseDate)));
            m.setPoster_path(cursor.getString(cursor.getColumnIndex(FavouriteColumns.PosterPath)));
            m.setVote_average(cursor.getString(cursor.getColumnIndex(FavouriteColumns.VoteAverage)));
            m.setOverview(cursor.getString(cursor.getColumnIndex(FavouriteColumns.Overview)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m;
    }
}
