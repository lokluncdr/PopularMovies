package com.example.lok.popularmovies.pojo;

/**
 * Created by Lok on 26/11/16.
 */
public class MovieReview {

    //FIELDS
    private String author;
    private String content;

    //CONSTRUCTOR
    public MovieReview(String author, String content) {
        this.author = author;
        this.content = content;
    }

    //METHODS
    @Override
    public String toString() {
        return "MovieReview{" +
                "author='" + author + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
