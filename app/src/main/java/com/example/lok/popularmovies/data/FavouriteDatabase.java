package com.example.lok.popularmovies.data;

import net.simonvt.schematic.annotation.Table;

/**
 * Created by Lok on 18/01/17.
 * We just need to specify the version, a name, the schema it has to use
 */

@net.simonvt.schematic.annotation.Database(
        version = FavouriteDatabase.VERSION
)
public final class FavouriteDatabase {

    //FIELDS
    public static final int VERSION = 1;
    @Table(FavouriteColumns.class)
    public static final String FAVOURITES = "Favourites";

    //CONSTRUCTOR
    private FavouriteDatabase() {
    }
}
