package com.example.lok.popularmovies.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lok.popularmovies.R;
import com.example.lok.popularmovies.pojo.MovieReview;

import java.util.List;

/**
 * Created by Lok on 26/11/16.
 */

public class ReviewAdapter extends ArrayAdapter<MovieReview> {
    //FIELDS
    private final String NOREVIEW = "00000";
    private Context context;
    private List<MovieReview> movieReviews;

    /**
     * Constructor
     *
     * @param context The current context.
     * @param reviews The trailers to represent in the ListView.
     */
    public ReviewAdapter(Context context, List<MovieReview> reviews) {
        super(context, 0, reviews);
        this.context = context;
        this.movieReviews = reviews;
    }

    //METHODS
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater = null;

        if (convertView == null) {
            //get the inflater and inflate the XML layout for each item
            inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_detail_review, null);

            // well set up the ViewHolder
            viewHolder = new ViewHolder();
            viewHolder.textViewAuthor = (TextView) convertView.findViewById(R.id.movie_detail_author);
            viewHolder.textViewReview = (TextView) convertView.findViewById(R.id.movie_detail_review);

            // store the holder with the view.
            convertView.setTag(viewHolder);
        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Gets the MovieTrailer object from the ArrayAdapter at the appropriate position
        MovieReview movieReview = movieReviews.get(position);

        //Dont set the Author and Review if there are no reviews
        if (movieReview.getAuthor() != NOREVIEW && movieReview.getContent() != NOREVIEW) {
            viewHolder.textViewAuthor.setText("Author: " + movieReview.getAuthor());
            viewHolder.textViewReview.setText(movieReview.getContent());
        } else {
            //get the inflater and inflate the XML layout for each item
            convertView = inflater.inflate(R.layout.ordinary_textview, null);

            //Set up the ViewHolder
            viewHolder.textViewNoReviews = (TextView) convertView.findViewById(R.id.movie_detail_ordinary_textview);
            viewHolder.textViewNoReviews.setText(R.string.no_reviews);
        }

        return convertView;
    }

    //Viewholder pattern for reusing views by holding references to the Id's of the views
    public static class ViewHolder {
        public TextView textViewAuthor;
        public TextView textViewReview;
        public TextView textViewNoReviews;
    }
}
