package com.example.lok.popularmovies;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lok.popularmovies.asynctasks.MovieAsyncTask;
import com.example.lok.popularmovies.data.PicassoCache;
import com.example.lok.popularmovies.pojo.Movie;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.lok.popularmovies.R.id.recyclerView;


/**
 * An activity representing a list of Movies. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link MovieDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */

/*
PopularMovies: Has 2 Activities. First is a MovieListActivity which shows a GridView Layout via an
extended RecyclerView.Adapter<RecyclerAdapter.ViewHolder> class called RecyclerAdapter (Inner Class
of MovieListActivity). Which handles the binding of view and data (in this case Images which are being
retrieved and cached through the use of third party api Picasso(Cache) (https://github.com/square/picasso))
and the onClick events when user clicks on a Movie. Through onItemSelected() (which handles the Spinner
Items: Popular Movies, Top Rated Movies and Favourite Movies) first it's being checked if Favourites
was selected, if so it's being retrieved from local SQLite Database if it was saved there before.
Next internet connection is being checked. If there's none, Movies will be retrieved from
SharedPreferences, if it was saved there before. Otherwise MovieAsyncTask is being called for
retrieving either the top 20 Popular Movies, or Top Rated Movies. onPause() saves the Recycler
Scroll Position and onSaveInstanceState() saves the current state. When on tablets with large
screen layouts of w900dp a MovieDetailFragment is being used when in Landscape mode without the
need to go to MovieDetailActivity first. Otherwise MovieDetailFragment is being used (after
MovieDetailActivity is being called and set in it's onCreate()) when user clicks on a movie when
in Portrait mode. In MovieDetailFragment's onCreate() MovieAsyncTask is being called for MovieTrailers
and MovieReviews. Which are later being added to a ListView via a TrailerAdapter and a ReviewAdapter
which are both extending ArrayAdapter. Only TrailerAdapter implements View.OnClickListener so user
can click on Trailer to play it in YouTube or in browser. There's also a MenuItem with a Share Intent
to share the first movie(trailer). And an option to save the Movie as a Favourite, which will then
be added to a local SQLite FavouriteDatabase via a FavouriteProvider which extends ContentProvider
(both Database and ContentProvider are generated via third party Schematic api
(https://github.com/SimonVT/schematic)).
 */
public class MovieListActivity extends AppCompatActivity implements MovieAsyncTask.MoviesAsyncResponse, AdapterView.OnItemSelectedListener {
    //FIELDS
    private static Bundle mBundleRecyclerViewState;
    private final String MOVIE = "Movie";
    //For retrieval from Shared Preferences
    private final String POPULAR = "com.example.lok.popularmovies.PREFERENCE_FILE_KEY_POPULAR";
    private final String TOPRATED = "com.example.lok.popularmovies.PREFERENCE_FILE_KEY_TOPRATED";
    //To verify what to retrieve in MovieAsyncTask
    private final String _POPULAR = "popular";
    private final String _TOP_RATED = "toprated";
    private final String FRAGMENT_TAG = "popular_movies_landscape";
    private final String KEY_RECYCLER_STATE = "recycler_state";
    boolean isInFavourites;
    boolean isPopular; //True -> Popular, False -> Top Rated
    private boolean mTwoPane; //Portrait- or Landscape mode
    private List<Movie> movieList;
    private Gson gson;
    private SharedPreferences sharedPreferences;
    private TextView ordinaryTextView;
    private SharedPreferences.Editor editor;
    private Toolbar toolbar;
    private Spinner spinner;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private GridLayoutManager mGridLayoutManager;
    private Movie movie;
    private Context context;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        context = getApplicationContext();

        //Set up Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());
        setSupportActionBar(toolbar);

        //Set up Spinner
        spinner = (Spinner) findViewById(R.id.movies_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.category, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        //Default to Popular Movies
        isPopular = true;
        isInFavourites = false;

        movieList = new ArrayList<>();

        //Set up RecyclerView
        mRecyclerView = (RecyclerView) findViewById(recyclerView);
        mGridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        //Show message when there are no movies in cache
        ordinaryTextView = (TextView) findViewById(R.id.movie_list_ordinary_textview);

        if (findViewById(R.id.movie_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            MovieDetailFragment fragment = new MovieDetailFragment();

            //Enter only when Screen was rotated before and movie was saved before
            if (savedInstanceState != null) {

                //Retrieve Movie for when screen was rotated
                movie = savedInstanceState.getParcelable(MOVIE);

                if (movie != null) {
                    Bundle arguments = new Bundle();
                    arguments.putParcelable(MOVIE, movie);

                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.movie_detail_container, fragment)
                            .commit();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        showFromCache(); //A3.
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Save RecyclerView scroll position
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = mRecyclerView.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    /**
     * <p>Callback method to be invoked when an item in this view has been
     * selected. This callback is invoked only when the newly selected
     * position is different from the previously selected position or if
     * there was no selected item.</p>
     * <p>
     * Impelmenters can call getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param parent   The AdapterView where the selection happened
     * @param view     The view within the AdapterView that was clicked
     * @param position The position of the view in the adapter
     * @param id       The row id of the item that is selected
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //Default to showing recyclerview
        ordinaryTextView.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);

        //First check if user clicked on Favourites, cause if so, skip the other paths to show movielist. (Show favourites from cache regardless of whether there's an internet connection or not)
        //FAVOURITES
        if (getResources().getStringArray(R.array.category)[2].equals(parent.getItemAtPosition(position).toString())) {
            isInFavourites = true;
            isPopular = false;
            showFromCache(); //A1.
        } else {
            //Show from cache
            if (!Utility.isNetworkAvailable(this)) {
                //Popular
                if (getResources().getStringArray(R.array.category)[0].equals(parent.getItemAtPosition(position).toString())) {
                    isInFavourites = false;
                    isPopular = true;
                }
                //Top Rated
                else if (getResources().getStringArray(R.array.category)[1].equals(parent.getItemAtPosition(position).toString())) {
                    isInFavourites = false;
                    isPopular = false;
                }
                showFromCache(); //A2.
            }
            //Retrieve from internet
            else {
                //Clicked on Popular
                if (getResources().getStringArray(R.array.category)[0].equals(parent.getItemAtPosition(position).toString())) {
                    isInFavourites = false; //Flag to indicate user has clicked on Favourites
                    isPopular = true;

                    //Retrieve Movies from Popular Movies with AsyncTask
                    MovieAsyncTask asyncTask = new MovieAsyncTask(this);
                    asyncTask.execute(_POPULAR);
                }
                //Clicked on Top Rated
                else if (getResources().getStringArray(R.array.category)[1].equals(parent.getItemAtPosition(position).toString())) {
                    isInFavourites = false; //Flag to indicate user has clicked on Favourites
                    isPopular = false;

                    //Retrieve Movies from Top Rated Movies with AsyncTask
                    MovieAsyncTask asyncTask = new MovieAsyncTask(this);
                    asyncTask.execute(_TOP_RATED);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        // Save current state
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(MOVIE, movie);
        }
    }


    /**
     * Callback method to be invoked when the selection disappears from this
     * view. The selection can disappear for instance when touch is activated
     * or when the adapter becomes empty.
     *
     * @param parent The AdapterView that now contains no selected item.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void saveToSharedPref(boolean popular) {
        this.isPopular = popular;

        if (isPopular) {
            sharedPreferences = context.getSharedPreferences(POPULAR, 0);
        } else {
            sharedPreferences = context.getSharedPreferences(TOPRATED, 0);
        }
        //If a preferences file by this name does not exist, it will be created when you retrieve
        // an editor (SharedPreferences.edit()) and then commit changes (Editor.commit()).
        editor = sharedPreferences.edit();
        //Save to Shared Preferences
        gson = new Gson();
        String movieJson = gson.toJson(movieList);

        if (isPopular) {
            editor.putString(POPULAR, movieJson);
        } else {
            editor.putString(TOPRATED, movieJson);
        }
        editor.apply();
    }

    public void showFromCache() {
        //A. Retrieve list from SharedPreferences (Which is used to view images already cached before by PicassoCache in MovieAdapter)
        movieList = retrieveMovieList();

        if (!Utility.isNetworkAvailable(this)) {
            Toast.makeText(this, "There's no internet connection", Toast.LENGTH_SHORT).show();
        }

        //When user has clicked on Favourites, but has unFavourited a movie and comes back to this listview, which has to be updated
        //Show different message when there are no favourites to be shown
        //Check if there even are favourites
        if (movieList.size() == 0) {
            //Favourite
            if (isInFavourites) {
                ordinaryTextView.setText(R.string.no_favourites);
                ordinaryTextView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
            }
            //Popular Movies
            else if (isPopular && !isInFavourites) {
                ordinaryTextView.setText(R.string.no_popular_movies);
                ordinaryTextView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
            }
            //Top Rated Movies
            else if (!isPopular && !isInFavourites) {
                ordinaryTextView.setText(R.string.no_top_rated_movies);
                ordinaryTextView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
            }
        } else {
            showMovies(movieList);
        }
    }

    public List<Movie> retrieveMovieList() {
        //Retrieve list
        gson = new Gson();
        String json = "";
        //Retrieve From SharedPreferences
        if (isPopular && isInFavourites == false) {
            //Retrieve POPULAR
            sharedPreferences = context.getSharedPreferences(POPULAR, 0);
            json = sharedPreferences.getString(POPULAR, "");

            movieList = gson.fromJson(json, new TypeToken<ArrayList<Movie>>() {
            }.getType());
        } else if (!isPopular && isInFavourites == false) {
            //Retrieve TOPRATED
            sharedPreferences = context.getSharedPreferences(TOPRATED, 0);
            json = sharedPreferences.getString(TOPRATED, "");

            movieList = gson.fromJson(json, new TypeToken<ArrayList<Movie>>() {
            }.getType());
        }
        //Retrieve From Database
        else if (isInFavourites) {
            //Retrieve FAVOURITES
            movieList = Utility.retrieveFavouritesFromDB(context);
        }

        //Make sure to to send back a null object
        if (movieList == null) {
            movieList = new ArrayList<Movie>();
        }
        return movieList;
    }

    public void showMovies(final List<Movie> movieList) {
        //Set up the adapter for the recyclerview
        mAdapter = new RecyclerAdapter(movieList);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);

        // restore RecyclerView scroll position
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void processFinishMovies(JSONObject movies) {
        mRecyclerView = null;
        mRecyclerView = (RecyclerView) findViewById(recyclerView);

        //1. Convert to ArrayList
        movieList = Utility.getMoviesFromJSON(movies);
        //2. Save MovieList to Shared Preferences, for retrieval later when there's no internet and images need to be shown from cache
        saveToSharedPref(isPopular);
        //3. Show movieList
        showMovies(movieList);
    }

    //INNER CLASSES
    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        //FIELDS
        private List<Movie> movies;

        //CONSTRUCTOR
        public RecyclerAdapter(List<Movie> movies) {
            this.movies = movies;
        }

        //METHODS
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_content, parent, false);
            return new ViewHolder(inflatedView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Movie movie = movies.get(position);
            holder.bindPhoto(movie);
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }

        //ANOTHER NESTED INNER CLASS
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private static final String MOVIE_KEY = "MOVIE";
            //FIELDS
            public Movie movie;
            public ImageView movieImage;

            //CONSTRUCTOR
            public ViewHolder(View view) {
                super(view);
                movieImage = (ImageView) view.findViewById(R.id.movie_list_item);
                view.setOnClickListener(this); //Attach onClick to this view of a movie
            }

            //METHODS
            public void bindPhoto(Movie movie) {
                this.movie = movie;

                PicassoCache.getPicassoInstance(getBaseContext()).load("http://image.tmdb.org/t/p/w342/" + movie.getPoster_path()).into(movieImage);
            }

            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {

                movie = movieList.get(getAdapterPosition());

                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putParcelable(MOVIE, movie);

                    MovieDetailFragment fragment = new MovieDetailFragment();
                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.movie_detail_container, fragment, FRAGMENT_TAG) // FRAGMENT_TAG = popular_movies_landscape
                            .commit();
                } else {
                    //Go to MovieDetailActivity and pass ArrayList of Movies with it
                    Intent intent = new Intent(MovieListActivity.this, MovieDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(MOVIE, movieList.get(getAdapterPosition()));
                    intent.putExtra(MOVIE, bundle);
                    startActivity(intent);
                }
            }
        }
    }
}