Followed a course on Udacity. This is the result of that. An app which shows the 20 Most Popular or 20 Top Rated Movies from 
The Movie Database https://www.themoviedb.org/ (Api Key is needed from this api) in a gridview. When a user clicks on a movie 
a DetailActivity is shown with more info about the movie and Movie Trailers, and Movie Reviews are being retrieved as well, 
when available. A Movie can be shared, a trailer can be viewed via the YouTube App or via the browser, and a movie can be saved 
as favourite as well.